const grpc = require('grpc')
const ld = require('lodash')
const path = require('path')
const fs = require('fs')

const { getDefinitions, unpromisifyMethods } = require('./common')

const defaults = {
  protoExt: '.proto',
  internalServiceName: 'internal',
  rpcHost: '0.0.0.0',
  rpcPort: 50051,
  protoPath: path.resolve(process.cwd(), 'src'),
  serviceName: 'default',
  serviceVersion: null
}

class Service {

  constructor(userConfig = {}) {
    const options = this.options = ld.defaultsDeep({}, userConfig, defaults)
    if (!options.serviceVersion) {
      throw new Error('options.serviceVersion expeted')
    }

    const grpcServer = this.grpcServer = new grpc.Server()

    this.discoveryObj = []

    /**
     * implement internal gRPC methods
     */
    const implementation = require('./spec/internal')({
      proto: this.discoveryObj,
      name: options.serviceName,
      version: options.serviceVersion,
      rpcVersion: require(path.resolve(__dirname, '..', 'package.json')).version
    })
    const internalDef = getDefinitions('proto-file', path.resolve(__dirname, 'spec', 'internal.proto'))
    grpcServer.addService(internalDef.internal.Internal.service, unpromisifyMethods(implementation))

    /**
     * load external gRPC methods
     */
    fs.readdirSync(options.protoPath).filter(item => path.extname(item) === options.protoExt).map(item => {

      const protoPath = path.resolve(options.protoPath, item)
      const serviceName = path.basename(item, options.protoExt)
      const serviceClass = serviceName.charAt(0).toUpperCase() + serviceName.slice(1)
      const serviceDef =  getDefinitions('proto-file', protoPath)

      let implementation
      try {
        implementation = require(path.resolve(options.protoPath, serviceName))
      } catch (err) {
        if (err.code === 'MODULE_NOT_FOUND' && err.message.includes(serviceName)) {
          err.message = `${serviceName}: module not found in directory with .proto spec`
        }
        throw err
      }

      // register service
      grpcServer.addService(
        serviceDef[serviceName] ? serviceDef[serviceName][serviceClass].service : serviceDef[serviceClass].service,
        unpromisifyMethods(implementation)
      )
      // add proto spec into discovery procedure
      this.discoveryObj.push({
        name: serviceName,
        class: serviceClass,
        proto: fs.readFileSync(protoPath, 'utf8')
      })
    })
  }

  async startAsync() {
    const options = this.options
    this.grpcServer.bind(`${options.rpcHost}:${options.rpcPort}`, grpc.ServerCredentials.createInsecure())
    this.grpcServer.start()

    // console.log(`server started (grpc=${options.rpcHost}:${options.rpcPort})`)
  }

  async stopAsync() {
    return new Promise(resolve => {
      this.grpcServer.tryShutdown(resolve)
    })
  }

}


module.exports = Service

const { grpcStatuses } = require('../..')

let itemsStream
const items = [{
  id: 123,
  title: 'test title',
  author: 'test author'
}]

function list () {
  return Promise.resolve(items)
}

function add (call) {
  const item = call.request
  items.push(item)
  if (itemsStream) {
    itemsStream.write(item)
  }
  return Promise.resolve()
}

function get (call) {
  const found = items.find(item => item.id === call.request.id)
  if (found) {
    return Promise.resolve(found)
  }
  const error = new Error('Element not found')
  error.code = grpcStatuses.NOT_FOUND
  throw error
}

function watch (stream) {
  itemsStream = stream
}

module.exports = { list, get, watch, add }

const assert = require('assert')
const wdw = require('..')
const grpc = require('grpc')
const path = require('path')

describe('service instance', () => {

  const ctx = {}
  const defaultOptions = {
    serviceName: 'test-service',
    serviceVersion: '1.1.2',
    protoPath: `${__dirname}/assets`
  }

  it('create service instance', async () => {
    ctx.service = new wdw.Service(defaultOptions)
    await ctx.service.startAsync()
  })

  it('get service info', cb => {
    const proto = grpc.load(path.resolve(__dirname, '..', 'src', 'spec', 'internal.proto'))
    const client = new proto.internal.Internal('localhost:50051', grpc.credentials.createInsecure())
    client.serviceInfo({}, (err, res) => {
      assert.ifError(err)
      assert.equal(res.name, defaultOptions.serviceName)
      assert.equal(res.version, defaultOptions.serviceVersion)
      assert.equal(res.rpcVersion, require('../package').version)
      cb()
    })
  })

  it('discover available methods', cb => {
    const proto = grpc.load(path.resolve(__dirname, '..', 'src', 'spec', 'internal.proto'))
    const client = new proto.internal.Internal('localhost:50051', grpc.credentials.createInsecure())
    client.discoverProto({}, (err, res) => {
      assert.ifError(err)
      assert.ok(Array.isArray(res.proto))
      const [ first ] = res.proto
      assert(first.name, 'test')
      assert(first.class, 'Test')
      assert(typeof first.proto, 'string')
      cb()
    })
  })

  it('ensure RPC works correct', cb => {
    const proto = grpc.load(path.resolve(__dirname, 'assets', 'test.proto'))
    const client = new proto.test.Test('localhost:50051', grpc.credentials.createInsecure())
    client.list({}, (err, res) => {
      assert.ifError(err)
      const [ first ] = res.item
      assert(first.title, 'test title')
      cb()
    })
  })


  after('finish', async () => {
    await ctx.service.stopAsync()
  })
})

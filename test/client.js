const assert = require('assert')
const wdw = require('..')

describe('client instance', () => {

  const ctx = {}
  const serviceOptions = {
    serviceVersion: '1.0.0',
    protoPath: `${__dirname}/assets`
  }

  before('create service', async () => {
    ctx.service = new wdw.Service(serviceOptions)
    await ctx.service.startAsync()
  })


  it('init client connection', async () => {
    ctx.client = new wdw.Client()
    await ctx.client.discoverAsync()
  })

  it('request remote method', async () => {
    const res = await ctx.client.service('test').listAsync({})
    assert.ok(Array.isArray(res.item))
    const [ first ] = res.item
    assert(first.id, 123)
  })


  after('finish', async () => {
    await ctx.service.stopAsync()
  })
})

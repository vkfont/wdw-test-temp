const bluebird = require('bluebird')
const grpc = require('grpc')
const ld = require('lodash')
const path = require('path')
const semver = require('semver')
const { getDefinitions } = require('./common')

const defaults = {
  rpcHost: 'localhost',
  rpcPort: 50051,
  serviceVersion: '>0.0.0',
  rpcVersion: '>0.0.0'
}

class Client {

  constructor(userConfig = {}) {
    this.options = ld.defaultsDeep({}, userConfig, defaults)
    this.clients = {}
  }

/**
 * Fetch .proto specifications from remote service and create set of according clients
 */
  async discoverAsync() {
    const options = this.options
    const endpoint = `${options.rpcHost}:${options.rpcPort}`
    const internalDef = getDefinitions('proto-file', path.resolve(__dirname, 'spec', 'internal.proto'))
    const internalClient = this.clients.internal = bluebird.promisifyAll(
      new internalDef.internal.Internal(endpoint, grpc.credentials.createInsecure())
    )

    const { version, rpcVersion } = await internalClient.serviceInfoAsync({})

    if (!semver.satisfies(version, options.serviceVersion)) {
      throw new Error(`endpoint "${endpoint}" service version is ${version} (${options.serviceVersion} expected)`)
    }

    if (!semver.satisfies(rpcVersion, options.rpcVersion)) {
      throw new Error(`endpoint "${endpoint}" rpc lib version is ${rpcVersion} (${options.rpcVersion} expected)`)
    }

    const { proto } = await internalClient.discoverProtoAsync({})
    if (!Array.isArray(proto)) {
      throw new Error(`endpoint "${endpoint}" is not correct service (.discoverProto fail)`)
    }

    proto.reduce((acc, item) => {
      // item.name, item.class
      const serviceDef = getDefinitions('proto-text', item.proto)
      acc[item.name] = bluebird.promisifyAll(
        new serviceDef[item.name][item.class](endpoint, grpc.credentials.createInsecure())
      )
      return acc
    }, this.clients)
  }

  service(name) {
    if (!this.clients[name]) {
      throw new Error(`service ${name} not found`)
    }
    return this.clients[name]
  }
}

module.exports = Client

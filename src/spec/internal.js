module.exports = options => {
  const { proto, name, version, rpcVersion } = options

  return {

/**
 * return stringified version of *.proto (should be added by service)
 */
    discoverProto() {
      return Promise.resolve(proto)
    },

    serviceInfo() {
      return Promise.resolve({ name, version, rpcVersion })
    }

  }
}

const grpc = require('grpc')

module.exports = {
  Service: require('./service'),
  Client: require('./client'),
  grpcStatuses: grpc.status
}

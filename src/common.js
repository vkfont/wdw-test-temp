const grpc = require('grpc')
const ProtoBuf = require('protobufjs')

function getDefinitions(type, input) {
  switch (type) {
    case 'proto-file':
      return grpc.load(input, 'proto')
    case 'proto-text':
      const proto = ProtoBuf.loadProto(input)
      return grpc.loadObject(proto.lookup())
    default:
      throw new Error(`unknown type ${type}`)
  }
}
// return proto[name][name.charAt(0).toUpperCase() + name.slice(1)]

/**
 * Convert promises into callback-style functions, suitable for gRPC server
 */
function unpromisifyMethods(promises) {
  return Object.keys(promises).reduce((acc, name) => {
    const method = promises[name]
    acc[name] = (call, callback) => {
      method(call).then(res => callback(null, res)).catch(err => {
        callback({
          detatils: err.message,
          code: err.code
        })
      })
    }
    return acc
  }, {})
}


module.exports = { getDefinitions, unpromisifyMethods }
